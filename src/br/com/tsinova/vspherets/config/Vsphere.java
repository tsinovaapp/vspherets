package br.com.tsinova.vspherets.config;

import java.util.List;

public class Vsphere {
    
    private Output output;
    private Beat beat;
    private List<SDK> sdks;

    public Vsphere() {
    }
    
    public Output getOutput() {
        return output;
    }

    public void setOutput(Output output) {
        this.output = output;
    }

    public Beat getBeat() {
        return beat;
    }

    public void setBeat(Beat beat) {
        this.beat = beat;
    }

    public List<SDK> getSdks() {
        return sdks;
    }

    public void setSdks(List<SDK> sdks) {
        this.sdks = sdks;
    }    
    
}
