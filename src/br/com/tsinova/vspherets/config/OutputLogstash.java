package br.com.tsinova.vspherets.config;

import org.json.JSONException;
import org.json.JSONObject;

public class OutputLogstash {

    private String host;
    private int port;

    public OutputLogstash() {
    }
    
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
}
