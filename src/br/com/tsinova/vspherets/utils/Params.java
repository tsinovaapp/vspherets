package br.com.tsinova.vspherets.utils;

import io.github.openunirest.http.JsonNode;
import io.github.openunirest.http.Unirest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class Params {

    public static Map<String, Object> getParamsEscala(String elasticsearchHost, String variable) throws Exception{

        String url = elasticsearchHost + "params/_search?q=variable:" + variable;

        JsonNode node = Unirest.get(url)
                .header("Content-Type", "application/json").asJson().getBody();

        if (node.getObject().has("error") || node.getObject().getJSONObject("hits").getInt("total") == 0) {
            return null;
        }

        Map<String, Object> map = new HashMap<>();

        JSONObject hit = node.getObject().getJSONObject("hits").getJSONArray("hits").getJSONObject(0);

        JSONObject source = hit.getJSONObject("_source");

        map.put("normal", source.get("normal"));
        map.put("attention", source.get("attention"));
        map.put("critical", source.get("critical"));
        map.put("id", hit.get("_id"));

        return map;

    }
    
    public static Map<String, Object> getParamsPlugin(String elasticsearchHost, String variable) throws Exception {

        String url = elasticsearchHost + "params/_search?q=variable:" + variable;

        JsonNode node = Unirest.get(url)
                .header("Content-Type", "application/json").asJson().getBody();

        if (node.getObject().has("error") || node.getObject().getJSONObject("hits").getInt("total") == 0) {
            return null;
        }

        Map<String, Object> map = new HashMap<>();

        JSONObject hit = node.getObject().getJSONObject("hits").getJSONArray("hits").getJSONObject(0);

        JSONObject source = hit.getJSONObject("_source");

        map.put("interval", source.get("interval"));
        map.put("id", hit.get("_id"));

        return map;

    }

    public static boolean writeParamsEscala(String elasticsearchHost, String variable, Double n1, Double n2, Double n3) throws Exception {

        Map<String, Object> params = getParamsEscala(elasticsearchHost, variable);

        String query = "{\"variable\":\"" + variable + "\",\"normal\":" + n1 + ",\"attention\":" + n2 + ",\"critical\":" + n3 + ", \"type\":\"scale\"}";

        // POST
        if (params == null) {

            JsonNode node = Unirest.post(elasticsearchHost + "params/doc")
                    .header("Content-Type", "application/json")
                    .body(query).asJson().getBody();

            return node.getObject().getString("result").equalsIgnoreCase("created");

        }

        // PUT
        JsonNode node = Unirest.put(elasticsearchHost + "params/doc/" + params.get("id"))
                .header("Content-Type", "application/json")
                .body(query).asJson().getBody();

        return node.getObject().getString("result").equalsIgnoreCase("updated");

    }
    
    public static boolean writeParamsPlugin(String elasticsearchHost, String variable, Integer interval)throws Exception {

        Map<String, Object> params = getParamsPlugin(elasticsearchHost, variable);

        String query = "{\"variable\":\"" + variable + "\",\"interval\":" + interval + ", \"type\":\"plugin\"}";

        // POST
        if (params == null) {

            JsonNode node = Unirest.post(elasticsearchHost + "params/doc")
                    .header("Content-Type", "application/json")
                    .body(query).asJson().getBody();

            return node.getObject().getString("result").equalsIgnoreCase("created");

        }

        // PUT
        JsonNode node = Unirest.put(elasticsearchHost + "params/doc/" + params.get("id"))
                .header("Content-Type", "application/json")
                .body(query).asJson().getBody();

        return node.getObject().getString("result").equalsIgnoreCase("updated");

    }

}