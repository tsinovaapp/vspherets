package br.com.tsinova.vspherets.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import org.json.JSONObject;

public class Util {

    public static double convertHertzToGigahertz(long value, int cores) {
        double newValue = (value / Math.pow(10, 9)) * cores;
        BigDecimal bd = new BigDecimal(newValue);
        bd = bd.setScale(1, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static double convertBytesToGB(long value) {
        double newValue = (value / 1073741824.0);
        BigDecimal bd = new BigDecimal(newValue);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static String getDatetimeNowForElasticsearch() {
        return Instant.now().toString();
    }

    public static String prettyPrinter(String value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                    mapper.readValue(value, Object.class));
        } catch (Exception ex) {
            return value;
        }
    }
    
    public static String prettyPrinter(JSONObject value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                    mapper.readValue(value.toString(), Object.class));
        } catch (Exception ex) {
            return value.toString();
        }
    }
    
    public static String prettyPrinter(Object value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                    value);
        } catch (Exception ex) {
            return value.toString();
        }
    }

}
