package br.com.tsinova.vspherets.models;

import br.com.tsinova.vspherets.utils.Util;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.mo.Datastore;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.VirtualMachine;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class HostSystemEsxi extends Host {
    
    public static final String TYPE = "HostSystem";
    private List<Storage> listStorage;
    private List<HostVirtualMachine> listVirtualMachine;
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();
        json.put("name", name);
        
        JSONObject device = new JSONObject();
        device.put("mac", macHost);
        device.put("ip", ipHost);
        
        json.put("device", device);                
        
        json.put("memory_size", memorySize);
        json.put("memory_usage", memoryUsage);
        json.put("memory_available", memoryAvailable);
        
        json.put("cpu_size", cpuSize);
        json.put("cpu_usage", cpuUsage);
        json.put("cpu_available", cpuAvailable);
        json.put("cpu_cores", cpuCores);
        
        json.put("type", TYPE);
        
        return json;
        
    }
    
    
    public HostSystemEsxi(HostSystem host) throws RuntimeFault, RemoteException {                        
        
        name = host.getName();
        ipHost = host.getConfig().getNetwork().getVnic()[0].getSpec().getIp().getIpAddress();
        macHost = host.getConfig().getNetwork().getVnic()[0].getSpec().getMac();
        
        memorySize = host.getHardware().getMemorySize();
        memoryUsage = host.getSummary().getQuickStats().getOverallMemoryUsage() * 1048576L;
        memoryAvailable = memorySize - memoryUsage;
        
        cpuCores = host.getHardware().getCpuInfo().getNumCpuCores();
        cpuSize = Util.convertHertzToGigahertz(host.getHardware().getCpuInfo().getHz(), cpuCores);
        
        try{
            cpuUsage = host.getSummary().getQuickStats().getOverallCpuUsage();
        }catch(Exception ex){
            cpuUsage = 0;  
        }
        
        cpuAvailable = (cpuSize * 1000) - cpuUsage;        
        
        listStorage = new ArrayList<>();
        for(Datastore datastore : host.getDatastores()){            
            Storage storage = Storage.getStorageByDatastore(datastore, ipHost, macHost);  
            storage.setTypeHost(TYPE);
            listStorage.add(storage);
        }        
        
        listVirtualMachine = new ArrayList<>();
        for(VirtualMachine virtualMachine : host.getVms()){
            listVirtualMachine.add(new HostVirtualMachine(virtualMachine, ipHost));
        }
        
    }
    
    public List<Storage> getListStorage() {
        return listStorage;
    }

    public void setListStorage(List<Storage> listStorage) {
        this.listStorage = listStorage;
    }   

    public List<HostVirtualMachine> getListVirtualMachine() {
        return listVirtualMachine;
    }

    public void setListVirtualMachine(List<HostVirtualMachine> listVirtualMachine) {
        this.listVirtualMachine = listVirtualMachine;
    }    
    
}