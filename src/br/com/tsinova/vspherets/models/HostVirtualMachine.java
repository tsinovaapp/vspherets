package br.com.tsinova.vspherets.models;

import br.com.tsinova.vspherets.utils.Util;
import com.vmware.vim25.GuestDiskInfo;
import com.vmware.vim25.RuntimeFault;
import com.vmware.vim25.VirtualDevice;
import com.vmware.vim25.VirtualEthernetCard;
import com.vmware.vim25.mo.VirtualMachine;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class HostVirtualMachine extends Host {

    public static final String TYPE = "VirtualMachine";
    private String hostName;
    private String os;
    private String ipHostParent;
    private int powerState;
    private List<Storage> listStorage;

    public JSONObject toJSONObject() throws JSONException {

        JSONObject json = new JSONObject();
        json.put("name", name);

        JSONObject device = new JSONObject();
        device.put("mac", macHost);
        device.put("ip", ipHost);
        device.put("host_name", hostName);
        device.put("os", os);
        device.put("ip_parent", ipHostParent);

        json.put("device", device);

        json.put("memory_size", memorySize);
        json.put("memory_usage", memoryUsage);
        json.put("memory_available", memoryAvailable);

        json.put("cpu_size", cpuSize);
        json.put("cpu_usage", cpuUsage);
        json.put("cpu_available", cpuAvailable);
        json.put("cpu_cores", cpuCores);

        json.put("type", TYPE);

        json.put("power_state", powerState);

        return json;

    }

    private String getMacAddress(VirtualMachine vm) {
        String macAddress = "";
        for (VirtualDevice vd : vm.getConfig().getHardware().getDevice()) {
            try {
                VirtualEthernetCard vEth = (VirtualEthernetCard) vd;
                macAddress = vEth.macAddress;
            } catch (Exception e) {
            }
        }
        return macAddress;
    }

    public HostVirtualMachine(VirtualMachine vm, String ipHostEsxi) throws RuntimeFault, RemoteException {

        this.powerState = vm.getRuntime().getPowerState().ordinal();
        this.ipHostParent = ipHostEsxi;

        name = vm.getName();
        hostName = vm.getGuest().getHostName();
        ipHost = vm.getGuest().getIpAddress();
        macHost = getMacAddress(vm);
        os = vm.getGuest().getGuestFullName();                
        
        memorySize = vm.getConfig().getHardware().getMemoryMB() * 1048576L;

        // maquina ligada
        if (powerState == 1) {
            memoryUsage = vm.getSummary().getQuickStats().getHostMemoryUsage();
            memoryUsage = memoryUsage > 0 ? memoryUsage * 1048576L : 0;
            memoryAvailable = memorySize - memoryUsage;
        } else {
            memoryUsage = 0;
            memoryAvailable = memorySize;
        }

        cpuCores = vm.getConfig().getHardware().getNumCPU();
        // maquina ligada
        if (powerState == 1) {
            try {
                cpuSize = vm.getRuntime().getMaxCpuUsage();
                if (cpuSize > 0) {
                    cpuSize = Util.round((cpuSize / 1000.0), 2);
                }
            } catch (Exception ex) {
                cpuSize = 0;
            }
            try {
                cpuUsage = vm.getSummary().getQuickStats().getOverallCpuUsage();
            } catch (Exception ex) {
                cpuUsage = 0;
            }
            cpuAvailable = (cpuSize * 1000) - cpuUsage;
        } else {
            cpuSize = 0;
            cpuUsage = 0;
            cpuAvailable = 0;
        }

        listStorage = new ArrayList<>();
        // maquina ligada ou suspensa
        if (powerState == 1) {
            if (vm.getGuest().getDisk() != null) {
                for (GuestDiskInfo guestDiskInfo : vm.getGuest().getDisk()) {                    
                    Storage storage = Storage.getStorageByGuestDiskInfo(guestDiskInfo, ipHost, macHost);
                    storage.setTypeHost(TYPE);                    
                    listStorage.add(storage);
                }
            }
        }

    }

    public HostVirtualMachine() {
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getIpHostParent() {
        return ipHostParent;
    }

    public void setIpHostParent(String ipHostParent) {
        this.ipHostParent = ipHostParent;
    }

    public List<Storage> getListStorage() {
        return listStorage;
    }

    public void setListStorage(List<Storage> listStorage) {
        this.listStorage = listStorage;
    }

}
