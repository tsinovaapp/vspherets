package br.com.tsinova.vspherets.models;

public class Host {

    protected String name;    
    protected String macHost;
    protected String ipHost;
    
    protected long memorySize; // em bytes
    protected long memoryUsage; // em bytes
    protected long memoryAvailable; // em bytes
    
    protected double cpuSize; // em gigaherts
    protected double cpuUsage; // em megaherts
    protected double cpuAvailable; // em megaherts
    protected int cpuCores; // número de núcleos    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacHost() {
        return macHost;
    }

    public void setMacHost(String macHost) {
        this.macHost = macHost;
    }

    public String getIpHost() {
        return ipHost;
    }

    public void setIpHost(String ipHost) {
        this.ipHost = ipHost;
    }

    public long getMemorySize() {
        return memorySize;
    }

    public void setMemorySize(long memorySize) {
        this.memorySize = memorySize;
    }

    public long getMemoryUsage() {
        return memoryUsage;
    }

    public void setMemoryUsage(long memoryUsage) {
        this.memoryUsage = memoryUsage;
    }

    public long getMemoryAvailable() {
        return memoryAvailable;
    }

    public void setMemoryAvailable(long memoryAvailable) {
        this.memoryAvailable = memoryAvailable;
    }

    public double getCpuSize() {
        return cpuSize;
    }

    public void setCpuSize(double cpuSize) {
        this.cpuSize = cpuSize;
    }

    public double getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(double cpuUsage) {
        this.cpuUsage = cpuUsage;
    }

    public double getCpuAvailable() {
        return cpuAvailable;
    }

    public void setCpuAvailable(double cpuAvailable) {
        this.cpuAvailable = cpuAvailable;
    }

    public int getCpuCores() {
        return cpuCores;
    }

    public void setCpuCores(int cpuCores) {
        this.cpuCores = cpuCores;
    }    
        
}