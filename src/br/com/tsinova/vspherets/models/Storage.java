package br.com.tsinova.vspherets.models;

import com.vmware.vim25.GuestDiskInfo;
import com.vmware.vim25.mo.Datastore;
import org.json.JSONException;
import org.json.JSONObject;

public class Storage {
        
    public static final String TYPE = "Storage";
    private String name;
    private String ipHost;
    private String macHost;
    private String typeHost;
    private String url;
    private long totalSpace; // total em bytes
    private long availableSpace; // total em bytes disponível
    private long usageSpace; // total em bytes utilizado   

    private Storage() {
    }       
    
    public JSONObject toJSONObject() throws JSONException{
        
        JSONObject json = new JSONObject();
        json.put("name", name);
        
        JSONObject device = new JSONObject();
        device.put("mac", macHost);
        device.put("ip", ipHost);
        device.put("type", typeHost);

        json.put("device", device);
                
        json.put("url", url);
        json.put("total_space", totalSpace);
        json.put("available_space", availableSpace);
        json.put("usage_space", usageSpace);
        json.put("type", TYPE);
        
        return json;
        
    }    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMacHost() {
        return macHost;
    }

    public void setMacHost(String macHost) {
        this.macHost = macHost;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(long totalSpace) {
        this.totalSpace = totalSpace;
    }

    public long getAvailableSpace() {
        return availableSpace;
    }

    public void setAvailableSpace(long availableSpace) {
        this.availableSpace = availableSpace;
    }

    public long getUsageSpace() {
        return usageSpace;
    }

    public void setUsageSpace(long usageSpace) {
        this.usageSpace = usageSpace;
    }

    public String getIpHost() {
        return ipHost;
    }

    public void setIpHost(String ipHost) {
        this.ipHost = ipHost;
    }

    public String getTypeHost() {
        return typeHost;
    }

    public void setTypeHost(String typeHost) {
        this.typeHost = typeHost;
    }    

    public static Storage getStorageByGuestDiskInfo(GuestDiskInfo storage, 
            String ipHostParent, String macHostParent){        
        Storage disk = new Storage();
        disk.setTotalSpace(storage.getCapacity());
        disk.setAvailableSpace(storage.getFreeSpace());
        disk.setUsageSpace(disk.getTotalSpace() - disk.getAvailableSpace());        
        disk.setName(storage.getDynamicType());
        disk.setUrl(storage.getDiskPath());   
        disk.setIpHost(ipHostParent);
        disk.setMacHost(macHostParent);
        return disk;
    }
    
    public static Storage getStorageByDatastore(Datastore storage, String ipHostParent, String macHostParent){        
        Storage disk = new Storage();
        disk.setTotalSpace(storage.getSummary().getCapacity());
        disk.setAvailableSpace(storage.getSummary().getFreeSpace());
        disk.setUsageSpace(disk.getTotalSpace() - disk.getAvailableSpace());        
        disk.setName(storage.getSummary().getName());
        disk.setUrl(storage.getSummary().getUrl());  
        disk.setIpHost(ipHostParent);
        disk.setMacHost(macHostParent);
        return disk;
    }      
    
}