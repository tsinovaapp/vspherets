package br.com.tsinova.vspherets;

import br.com.tsinova.vspherets.config.SDK;
import br.com.tsinova.vspherets.utils.Params;
import br.com.tsinova.vspherets.config.Vsphere;
import br.com.tsinova.vspherets.utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Service extends Thread {

    private final List<ReadSend> listReadSends;
    private Vsphere vsphere;

    public Service() {
        listReadSends = new ArrayList<>();
    }

    @Override
    public void run() {

        Logger.getLogger(Service.class.getName())
                .log(Level.INFO, "Reading parameters ...");

        try (InputStream inputStream = new FileInputStream("vspherets.json")) {

            ObjectMapper objectMapper = new ObjectMapper();

            vsphere = objectMapper.readValue(inputStream, Vsphere.class);

            Logger.getLogger(Service.class.getName())
                .log(Level.INFO, Util.prettyPrinter(vsphere));
            
            Logger.getLogger(Service.class.getName())
                    .log(Level.INFO, "Successful reading");

        } catch (Exception ex) {
            Logger.getLogger(Service.class.getName())
                    .log(Level.WARNING, "Failed to read", ex);
            return;

        }

        while (true) {

            Map<String, Object> plugin = null;

            try {
                plugin = Params.getParamsPlugin(vsphere.getBeat().getElasticsearchHost(), "vsphere_params_plugin");
            } catch (Exception ex) {
            }

            Integer interval = (plugin != null
                    && plugin.containsKey("interval")) ? Integer.parseInt(plugin.get("interval").toString())
                    : null;

            if (interval != null) {

                Iterator<ReadSend> it = listReadSends.iterator();

                while (it.hasNext()) {

                    ReadSend readSend = it.next();

                    if (readSend.sdk.getInterval() != interval) {

                        try {
                            readSend.close();
                            readSend.interrupt();
                            readSend.join();
                            Logger.getLogger(Service.class.getName())
                                    .log(Level.INFO, "Closed read " + readSend.sdk.getUrl());
                        } catch (Exception ex) {
                        }

                        it.remove();

                    }

                }

            }

            for (SDK switche : vsphere.getSdks()) {

                boolean hasExists = listReadSends.stream()
                        .anyMatch(rs -> rs.sdk.getUrl().equals(switche.getUrl())
                        && rs.sdk.getInterval() == switche.getInterval());

                if (!hasExists) {

                    if (interval != null) {
                        switche.setInterval(interval); // define o novo intervalo                            
                    }

                    ReadSend readSend = new ReadSend(switche, vsphere.getOutput(), vsphere.getBeat());
                    readSend.start();
                    listReadSends.add(readSend);

                    Logger.getLogger(Service.class.getName())
                                    .log(Level.INFO, "Read and send processes successfully started "
                            + "for host " + switche.getUrl());                    

                }

            }

            try {
                Thread.sleep(3000);
            } catch (Exception ex) {
            }

        }

    }

    public static void main(String[] args) {
        Service service = new Service();
        service.start();
    }

}
