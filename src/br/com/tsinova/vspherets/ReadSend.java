package br.com.tsinova.vspherets;

import br.com.tsinova.vspherets.utils.Util;
import br.com.tsinova.vspherets.models.Storage;
import br.com.tsinova.vspherets.config.SDK;
import br.com.tsinova.vspherets.config.Output;
import br.com.tsinova.vspherets.models.HostVirtualMachine;
import br.com.tsinova.vspherets.models.HostSystemEsxi;
import br.com.tsinova.vspherets.config.Beat;
import br.com.tsinova.vspherets.config.OutputLogstash;
import com.vmware.vim25.mo.Folder;
import com.vmware.vim25.mo.HostSystem;
import com.vmware.vim25.mo.InventoryNavigator;
import com.vmware.vim25.mo.ManagedEntity;
import com.vmware.vim25.mo.ServiceInstance;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class ReadSend extends Thread {

    public final SDK sdk;
    private final Output output;
    public final Beat beat;
    private boolean run;

    public ReadSend(SDK sdk, Output output, Beat beat) {
        this.sdk = sdk;
        this.output = output;
        this.run = true;
        this.beat = beat;
    }

    public void close() {
        this.run = false;
    }

    private JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    @Override
    public void run() {

        while (run) {

            Logger.getLogger(ReadSend.class.getName())
                    .log(Level.INFO, "Reading the vm " + sdk.getUrl() + ", Interval " + sdk.getInterval() + "s");

            List<HostSystemEsxi> listHostSystemEsxi = new ArrayList<>();

            try {

                ServiceInstance si = new ServiceInstance(new URL(
                        sdk.getUrl()), sdk.getUser(), sdk.getPassword(), true);

                Folder rootFolder = si.getRootFolder();

                ManagedEntity[] managedEntitys = new InventoryNavigator(rootFolder)
                        .searchManagedEntities(HostSystemEsxi.TYPE);

                for (ManagedEntity managedEntity : managedEntitys) {
                    listHostSystemEsxi.add(new HostSystemEsxi((HostSystem) managedEntity));
                }

                si.getServerConnection().logout();

            } catch (Exception ex) {
                Logger.getLogger(ReadSend.class.getName())
                        .log(Level.WARNING, "Could not fetch information from " + sdk.getUrl(), ex);
                try {
                    Thread.sleep(sdk.getInterval() * 1000);
                } catch (InterruptedException ex2) {
                }
                continue;

            }

            Logger.getLogger(ReadSend.class.getName())
                    .log(Level.INFO, "Search performed on " + sdk.getUrl() + " successfully");

            // Seta outros atributos, atributos que qualquer documento tem
            JSONObject jsonDefault = new JSONObject();

            try {

                jsonDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                JSONObject metadataJSON = new JSONObject();
                metadataJSON.put("beat", beat.getName());
                metadataJSON.put("version", beat.getVersion());
                jsonDefault.put("@metadata", metadataJSON);

                JSONObject beatJSON = new JSONObject();
                beatJSON.put("name", beat.getName());
                beatJSON.put("version", beat.getVersion());
                jsonDefault.put("beat", beatJSON);

                jsonDefault.put("tags", beat.getTags());

            } catch (JSONException ex) {
            }

            OutputLogstash out = output.getLogstash();

            Logger.getLogger(ReadSend.class.getName())
                    .log(Level.INFO, "Sending data to " + out.getHost() + ":" + out.getPort());

            for (HostSystemEsxi hostSystemEsxi : listHostSystemEsxi) {

                try {

                    // envia os dados do HOST SYSTEM
                    JSONObject jsonHostSystemEsxi = getJson(jsonDefault, hostSystemEsxi.toJSONObject());
                    Logger.getLogger(ReadSend.class.getName())
                            .log(Level.INFO, Util.prettyPrinter(jsonHostSystemEsxi));
                    try (Socket socket = new Socket(out.getHost(), out.getPort());
                            DataOutputStream os = new DataOutputStream(
                                    new BufferedOutputStream(socket.getOutputStream()))) {
                        os.writeBytes(jsonHostSystemEsxi.toString());
                        os.flush();
                    } catch (Exception ex) {
                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.WARNING, "Failed to send jsonHostSystemEsxi for " + hostSystemEsxi.getIpHost(), ex);

                    }

                    jsonHostSystemEsxi.put("type", "Latest" + HostSystemEsxi.TYPE);
                    jsonHostSystemEsxi.put("id_doc", "hostsystemesxi_" + hostSystemEsxi.getMacHost() + "_latest_" + HostSystemEsxi.TYPE.toLowerCase());

                    Logger.getLogger(ReadSend.class.getName())
                            .log(Level.INFO, Util.prettyPrinter(jsonHostSystemEsxi));
                    try (Socket socket = new Socket(out.getHost(), out.getPort());
                            DataOutputStream os = new DataOutputStream(
                                    new BufferedOutputStream(socket.getOutputStream()))) {
                        os.writeBytes(jsonHostSystemEsxi.toString());
                        os.flush();
                    } catch (Exception ex) {
                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.WARNING, "Failed to send jsonHostSystemEsxiLatest for " + hostSystemEsxi.getIpHost(), ex);

                    }

                    // envia o storage do HOST SYSTEM
                    for (Storage storage : hostSystemEsxi.getListStorage()) {

                        JSONObject storageHostSystemEsxi = getJson(jsonDefault, storage.toJSONObject());

                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.INFO, Util.prettyPrinter(storageHostSystemEsxi));
                        try (Socket socket = new Socket(out.getHost(), out.getPort());
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(storageHostSystemEsxi.toString());
                            os.flush();
                        } catch (Exception ex) {
                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.WARNING, "Failed to send storageHostSystemEsxi for " + hostSystemEsxi.getIpHost(), ex);

                        }

                        storageHostSystemEsxi.put("type", "Latest" + Storage.TYPE);
                        storageHostSystemEsxi.put("id_doc", "hostsystemesxi_" + storage.getMacHost() + "_storage_" + storage.getUrl().toLowerCase() + "_latest_" + Storage.TYPE.toLowerCase());

                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.INFO, Util.prettyPrinter(storageHostSystemEsxi));
                        try (Socket socket = new Socket(out.getHost(), out.getPort());
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(storageHostSystemEsxi.toString());
                            os.flush();
                        } catch (Exception ex) {
                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.WARNING, "Failed to send storageHostSystemEsxiLatest for " + hostSystemEsxi.getIpHost(), ex);

                        }

                    }

                    // envia os dados das VIRTUAL MACHINE
                    for (HostVirtualMachine hostVirtualMachine : hostSystemEsxi.getListVirtualMachine()) {

                        JSONObject jsonHostVirtualMachine = getJson(jsonDefault, hostVirtualMachine.toJSONObject());

                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.INFO, Util.prettyPrinter(jsonHostVirtualMachine));
                        try (Socket socket = new Socket(out.getHost(), out.getPort());
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(jsonHostVirtualMachine.toString());
                            os.flush();
                        } catch (Exception ex) {
                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.WARNING, "Failed to send jsonHostVirtualMachine for " + hostSystemEsxi.getIpHost(), ex);

                        }

                        jsonHostVirtualMachine.put("type", "Latest" + HostVirtualMachine.TYPE);
                        jsonHostVirtualMachine.put("id_doc", "virtualmachine_" + hostVirtualMachine.getMacHost() + "_latest_" + HostVirtualMachine.TYPE.toLowerCase());

                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.INFO, Util.prettyPrinter(jsonHostVirtualMachine));
                        try (Socket socket = new Socket(out.getHost(), out.getPort());
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(jsonHostVirtualMachine.toString());
                            os.flush();
                        } catch (Exception ex) {
                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.WARNING, "Failed to send jsonHostVirtualMachineLatest for " + hostSystemEsxi.getIpHost(), ex);

                        }

                        // envia o storage da VIRTUAL MACHINE
                        for (Storage storage : hostVirtualMachine.getListStorage()) {

                            JSONObject storageHostVirtualMachine = getJson(jsonDefault, storage.toJSONObject());

                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.INFO, Util.prettyPrinter(storageHostVirtualMachine));
                            try (Socket socket = new Socket(out.getHost(), out.getPort());
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(storageHostVirtualMachine.toString());
                                os.flush();
                            } catch (Exception ex) {
                                Logger.getLogger(ReadSend.class.getName())
                                        .log(Level.WARNING, "Failed to send storageHostVirtualMachine for " + hostSystemEsxi.getIpHost(), ex);

                            }

                            storageHostVirtualMachine.put("type", "Latest" + Storage.TYPE);
                            storageHostVirtualMachine.put("id_doc", "virtualmachine_" + storage.getMacHost() + "_storage_" + storage.getUrl().toLowerCase() + "_latest_" + Storage.TYPE.toLowerCase());

                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.INFO, Util.prettyPrinter(storageHostVirtualMachine));
                            try (Socket socket = new Socket(out.getHost(), out.getPort());
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(storageHostVirtualMachine.toString());
                                os.flush();
                            } catch (Exception ex) {
                                Logger.getLogger(ReadSend.class.getName())
                                        .log(Level.WARNING, "Failed to send storageHostVirtualMachineLatest for " + hostSystemEsxi.getIpHost(), ex);

                            }

                        }

                    }

                } catch (Exception ex) {
                    Logger.getLogger(ReadSend.class.getName())
                            .log(Level.WARNING, "Failed to forward HostSystem data " + hostSystemEsxi.getIpHost(), ex);

                }

            }

            Logger.getLogger(ReadSend.class.getName())
                    .log(Level.INFO, "Data sent to " + out.getHost() + ":" + out.getPort());

            Logger.getLogger(ReadSend.class.getName())
                    .log(Level.INFO, "Waiting time to search again ...");

            try {
                Thread.sleep(sdk.getInterval() * 1000);
            } catch (Exception ex) {
            }

        }

    }

}
